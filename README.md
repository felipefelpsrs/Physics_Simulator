# **Physics Simulator**

Phisics Simulation é um projeto com a finalidade de estudar o comportamento e trajetória de uma bola que é solta de um determinado ponto de coordenadas _X_ e _Y_.

### Sistema Operacional

O projeto foi todo implementado em Sistema Operacional Linux, Ubuntu 17.04.


### Compilador
#### Especificações do terminal

>>>	compilador g++ (Ubuntu 6.3.0-12ubuntu2) 6.3.0 20170406
	Copyright (C) 2016 Free Software Foundation, Inc.
	This is free software; see the source for copying conditions.  There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR 		A PARTICULAR PURPOSE.



#### O projeto será dividido em três partes, a primeira parte como *Bouncing Ball* a segunda como *Spring-Mass* e a etapa final, Spring-Mass_Graphics.



## Bouncing Ball

Onde é feito a análise do comportamento de uma bola atibuindo-a velocidade, onde tem-se apenas a aceleração da gravidade como força externa. E a partir disso, estuda-se o comportamento da bola em posições dadas em forma de coordenadas cartesianas _(x, y)_. 


### Método de compilação usado

>	O *comand line* foi feito através de um **Makefile**, o qual está também incluido nos arquivos do projeto.
>>	Comando usado: *make test-ball*, o qual executa a seguinte linha no terminal
>>>		$ g++     test-ball.cpp ball.cpp ball.h  -lGL -lGLU -lglut -o test-ball



### Arquivos e suas finalidades

*	**test-ball.cpp** -> onde contêm a função principal e que chama pra si a classe _ball.h_;
*	**ball.h** -> a qual é a classe filha de Simulation;
*	**ball.cpp** -> contém a implementação dos métodos herdados de _ball.h_;
*	**Simulation** -> é uma classe pai que constrói os metódos _step_ e _display_ em modo virtual;
*	**ghaphic.tex** -> programa implementado para gerar o gráfico;
*	**Makefile** -> arquivo com comandos de compliação que é chamdo no terminal para compilação dos arquivos _.h_ e _.cpp_ do projeto;
*	**test-ball-graphics.cpp** -> abre através do terminal uma representação do comportamento da bola, em forma de um gráfico.

### Diagrama de Classes

![alt text](https://gitlab.com/felipefelpsrs/Physics_Simulator/raw/master/Work%201/diagram-bouncing_ball.jpg)



### Saída do programa

Como saída, temos duas colunas que representam as coordernadas _X_ e _Y_ que representam a posição da bola. A quantidade de interações é 
definida em **test-ball.cpp**, nesse exemplo foi usado 2500 interações, ou seja, foram gerados 2500 pontos de posição da bola.

#### Exemplo de saída

	Entre com a posição inicial da bola em X: 0.1
	Entre com a posição inicial da bola em Y: 0.4
	0.103 0.39851
	0.106 0.39604
	0.109 0.39259
	0.112 0.38816
	0.115 0.38275
	0.118 0.37636
	0.121 0.36899
	0.124 0.36064
	0.127 0.35131
	0.13 0.341
	0.133 0.32971
	0.136 0.31744
	0.139 0.30419
	0.142 0.28996
	0.145 0.27475
	0.148 0.25856
	0.151 0.24139
	0.154 0.22324
	0.157 0.20411
	0.16 0.184
	0.163 0.16291
	0.166 0.14084
	0.169 0.11779
	0.172 0.09376
	0.175 0.06875
	0.178 0.04276
	0.181 0.01579
	0.184 -0.01216
	0.187 -0.04109
	0.19 -0.071
	0.193 -0.10189
	0.196 -0.13376
	0.199 -0.16661
	0.202 -0.20044
	0.205 -0.23525
	0.208 -0.27104
	0.211 -0.30781
	0.214 -0.34556
	0.217 -0.38429
	0.22 -0.424
	0.223 -0.46469
	0.226 -0.50636
	0.229 -0.54901
	0.232 -0.59264
	0.235 -0.63725
	0.238 -0.68284
	0.241 -0.72941
	0.244 -0.77696
	0.247 -0.82549
	0.25 -0.875
	0.253 -0.875
	0.256 -0.82549
	0.259 -0.77696
	0.262 -0.72941
	0.265 -0.68284
	0.268 -0.63725
	0.271 -0.59264
	0.274 -0.54901
	0.277 -0.50636
	0.28 -0.46469
	0.283 -0.424
	0.286 -0.38429
	0.289 -0.34556
	0.292 -0.30781
	0.295 -0.27104
	0.298 -0.23525
	0.301 -0.20044
	0.304 -0.16661
	0.307 -0.13376
	0.31 -0.10189
	0.313 -0.071
	0.316 -0.04109
	0.319 -0.01216
	0.322 0.01579
	0.325 0.04276
	0.328 0.06875
	0.331 0.09376
	0.334 0.11779
	0.337 0.14084
	0.34 0.16291
	0.343 0.184
	0.346 0.20411
	0.349 0.22324
	0.352 0.24139
	0.355 0.25856
	0.358 0.27475
	0.361 0.28996
	0.364 0.30419
	0.367 0.31744
	0.37 0.32971
	0.373 0.341
	0.376 0.35131
	0.379 0.36064
	0.382 0.36899
	0.385 0.37636
	0.388 0.38275
	0.391 0.38816
	0.394 0.39259
	0.397 0.39604
	0.4 0.39851

### Gerando o gráfico

Para gerar o gráfico, foi utilizado a plataforma _LaTex_.

Para a instalação, basta executar respectivamente os comandos em seu terminal do Ubuntu

>		$ sudo apt-get install texlive-full

>		$ sudo apt-get install texmaker

Após a instalação, você terá o editor de texto,  _TexMaker_, instalado em seu Sistema Operacional. E através do terminal, será possível executar os comandos para se gerar o gráfico, depois de ter o algoritmo pronto pra ser executado.

#### Procedimentos utilizados para plotagem do gráfico:

Primeiramente foi salvo a saída dos pontos dirtamente em um arquivo _txt_, em **output_piping.txt**, utilizando o comando
		
>		$ ./test-ball > output_piping.txt

Com o **output_piping.txt** gerado, pôde-se manipula-lo em um algoritmo em *LaTex*, o qual está sendo demonstrado logo abaixo, 


```latex
\documentclass[a4paper]{article}
	 
\usepackage[utf8]{inputenc} 
\usepackage[brazil]{babel}
\usepackage{pgfplots}   % pacote para uso do pgfplots
\usepackage{pgfplotstable}
 
\begin{document}
\begin{tikzpicture} %  indicação que estamos iniciando uma figura
\begin{axis}[ %  iniciando os eixos
xlabel=x,            % nomeando os eixos
ylabel=y]
	 
\addplot table [x index = {0}, y index = {1},col sep=space]{nome_arquivo.txt};

\end{axis}
	 
\end{tikzpicture}
\end{document}
```

onde basta adicionar no espaço entre chaves o arquivo em _txt_, (*{nome_arquvo.txt}*), e que o arquivo _txt_ esteja no mesmo diretório que o arquivo do algoritmo _.tex_.
que gerou o gráfico que esperava-se através da execussão do seguinte comando no terminal
	
>		$ pdflatex graphic.tex
	
que por sua vez, criou um *PDF* onde era plotado o gráfico, *graphic.pdf*.


#### Imagem do gráfico gerado no _LaTex_:

![alt text](https://gitlab.com/felipefelpsrs/Physics_Simulator/raw/master/Work%201/graphic_bouncingball.png)



## Spring-Mass

Onde é criado uma simulação dinâmica de um sistema massa-mola, que por sua vez é composto por duas massas nas extremidades de uma mola.

### Método de compilação utilizado

>	O *comand line* foi feito através de um **Makefile**, que ainda é o mesmo _Makefile_ do início do projeto, o qual inclui também a primeira parte do projeto, o _Bouncing Ball_.
>>	O comando passado ao terminal é um _make test-springmass_, o qual executa a seguinte linha no terminal
>>>		$ g++     test-springmass.cpp springmass.cpp  -lGL -lGLU -lglut -o test-springmass


### Arquivos e suas finalidades

*	**simulation.h** -> onde são inicializados os construtores _step()_ e _display_.
*	**springmass.h** -> contém a biblioteca _simulation.h_; Onde está definido os valores das gravidades utilizadas no projeto; Onde também foram criadas as classes _Vector2_, _Mass_, _Spring_ e _Springmass_, que por sua vez, herda da classe _Simulation_.
*	**springmass.cpp** -> contém a biblioteca _simulatiom.h_; Onde foi implementados os métodos herdados de _Springmass.h_.
*	**test-springmass.cpp** -> Onde se encontra a função principal, _main()_, a qual chama e passa os parâmetros e contola a quantidade de interações a serem feitas no projeto.
*	**ghaphic.tex** -> programa implementado para gerar o gráfico;
*	**Makefile** -> arquivo com comandos de compliação que é chamdo no terminal para compilação dos arquivos _.h_ e _.cpp_ do projeto;



### Diagrama de Classes

![alt text](https://gitlab.com/felipefelpsrs/Physics_Simulator/raw/master/Work%201/diagram-ball_spring.jpg)



### Saídas geradas

##### A seguir encontra-se um pequeno exemplo das saídas das coordenadas das massas do sistema, onde a primeira coluna é referente a _x_ e a segunda a _y_.

	-0.201505 0.595525
	0.201505 0.0917636
	-0.200007 0.582111
	0.200007 0.067045
	-0.195459 0.559788
	0.195459 0.0258125
	-0.187885 0.52862
	0.187885 -0.0319978
	-0.177369 0.488708
	0.177369 -0.106486
	-0.164047 0.440182
	0.164047 -0.197782
	-0.148105 0.38318
	0.148105 -0.306025
	-0.129788 0.317814
	0.129788 -0.431325
	-0.109399 0.244123
	0.109399 -0.573723
	-0.0872963 0.162014
	0.0872963 -0.733125
	-0.0638779 0.0712122
	0.0638779 -0.909257
	-0.0395485 -0.0287876
	0.0395485 -0.909257
	-0.0140289 -0.146184
	0.0140289 -0.724926
	0.0100144 -0.287069
	-0.0100144 -0.54253
	0.0332288 -0.447397
	-0.0332288 -0.366113
	0.0654758 -0.63308
	-0.0654758 -0.189763
	0.0757331 -0.853817
	-0.0757331 -0.00378119
	0.0772627 -0.853817
	-0.0772627 0.191827
	0.0729751 -0.619391
	-0.0729751 0.383373
	0.0692347 -0.394355
	-0.0692347 0.558888
	0.0659874 -0.179135
	-0.0659874 0.718796
	0.0631686 0.0257151
	-0.0631686 0.863651
	0.0607007 0.219512
	-0.0607007 0.863651
	0.0575615 0.406343
	-0.0575615 0.714708
	0.0563501 0.588132
	-0.0563501 0.545384
	0.0668681 0.763135
	-0.0668681 0.357425
	0.0545818 0.940206
	-0.0545818 0.141976
	0.0324356 0.940206
	-0.0324356 -0.108082
	0.00605936 0.752831
	-0.00605936 -0.382446
	-0.0205172 0.554487
	0.0205172 -0.671262



### Gráfico de tragetórias das massas gerado em _LaTex_

Para gerar o gráfico foi salvo a saída dos pontos em **output_springmass.txt** utilizando o comando
		
>		$ ./test-ball > output_springmass.txt

Com o **output_springmass.txt** gerado, pôde-se manipula-lo em um programa em *LaTex*, o qual foi o mesmo utilizado para gera o gráfico da primeira parte do projeto, _Bouncing Ball_. Apenas foi modificado a informação de qual arquivo ele deveria retirar as informações, sendo usado dessa vez o *output_springmass.txt*. E assim gerou-se o *graphic_springmass.pdf* como também ateriormente feito na primeira parte do projeto.

Comando usado para ploat o gráfico no _PDF_:

>		$ pdflatex graphic.tex

##### Gráfico:

![alt text](https://gitlab.com/felipefelpsrs/Physics_Simulator/raw/master/Work%201/graphic_test_springmass.png)


## Spring-Mass-Graphics

Nessa etapa é onde usamos os dados anteriores de Bouncing-Ball e Spring-Mass implementando agora de maneira a visualizar o deslocamento do sistema massa mola em tempo real, ou seja, de maneira animada gerada instantaneamente no gráfico.

Para isso será necessário instalar OpenGL e GLUT.

#### Instalando OpenGL e GLUT

Para a instalação do OpenGL e do GLUT, deve ser executado o comando a seguir no terminal:

>		$ sudo apt-get install freeglut3-dev


Mais descrições quanto ao OpenGL e o GLUT podem ser encontradas acessando os links abaixo:

* https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/
* http://www.opengl-tutorial.org/

### Método de compilação usado

>	O *comand line* também foi feito através do mesmo **Makefile** usado anteriormente nas duas etapas passadas.
>>	Comando usado: *make test-spring-graphics*, o qual executa a seguinte linha no terminal
>>>		$ g++     graphics.cpp springmass.cpp test-springmass-graphics.cpp



### Arquivos e suas finalidades

*	**graphics.h** -> onde encontra-se a classe Figure que herda da classe Drawable que instancia os métodos para criação do gráfico;
*	**graphics.cpp** -> onde é implementado os métodos que vem do halted _graphics,h_;
*	**test-ball-graphics.cpp** -> onde é definido a classe _BallDrawable_ e a função principal para execução de _Bouncing ball_;
*	**test-springmass-graphics.cpp** -> onde é definido a classe _SpringMassDrawable_ e onde é passado os parâmetros dos métodos para se gerar o gráfico;
*	**simulation.h** -> onde são inicializados os construtores _step()_ e _display_.
*	**ghaphic.tex** -> programa implementado para gerar o gráfico;
*	**Makefile** -> arquivo com comandos de compliação que é chamdo no terminal para compilação dos arquivos _.h_ e _.cpp_ do projeto;



### Saída do Programa

A imagem a seguir representa a execução do programa juntamente com o gráfico gerado em tempo real.


![alt text](https://gitlab.com/felipefelpsrs/Physics_Simulator/raw/master/Work%201/execucao_test-springmass-graphics.png)



### Diagrama de Classes

#### A seguir encontra-se um esquema representativo do Diagrama de Classes final do projeto:

![alt text](https://gitlab.com/felipefelpsrs/Physics_Simulator/raw/master/Work%201/esquema_diagrama_final.jpg)



#### Na imagem abaixo temos o Diagrama de Classes final propriamente dito:

![alt text](https://gitlab.com/felipefelpsrs/Physics_Simulator/raw/master/Work%201/diagram-ball_spring_graphics.jpg)



### Diagrama de Sequência

![alt text]()



































