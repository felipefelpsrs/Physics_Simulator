/** file: test-springmass-graphics.cpp
 ** brief: Tests the spring mass simulation with graphics
 ** author: Andrea Vedaldi
 ** modified by: Felipe Luis
 **/

#include "graphics.h"
#include "springmass.h"

#include <iostream>
#include <sstream>
#include <iomanip>

/* ---------------------------------------------------------------- */
class SpringMassDrawable : public SpringMass, public Drawable
/* ---------------------------------------------------------------- */
{

/* INCOMPLETE: TYPE YOUR CODE HERE */

private: 
	Figure figure;

public:
	SpringMassDrawable()
	: figure("Spring Mass")		// declarando o nome do gráfico gerado
	{
		figure.addDrawable(this);
	}

	void draw()
	{
	  	for(int i=0; i < vector_mass.size(); i++)     // imprimindo todas as massas
	 	{
			figure.drawCircle(vector_mass[i]->getPosition().x, vector_mass[i]->getPosition().y, vector_mass[i]->getRadius());
	 	}
	
	 	for(int j=0; j < vector_spring.size(); j++)
		{
			figure.drawLine(vector_spring[j]->getMass1()->getPosition().x, vector_spring[j]->getMass1()->getPosition().y, 
							vector_spring[j]->getMass2()->getPosition().x, vector_spring[j]->getMass2()->getPosition().y, 0);
							//nem faz diferença passar o parâmetro do raio... sempre gera da mesma maneira.	
		}
	}

	void display()
	{
		figure.update();
	}

} ;

int main(int argc, char** argv)
{
  glutInit(&argc,argv) ;

  /* INCOMPLETE: TYPE YOUR CODE HERE */

  // bastou pegar os parâmetros declarados na main() de test-springmass.cpp, seguiu a mesma ideia 

  SpringMassDrawable springmass ;

  double mass = 1000;			// acrescentando um valor maior a massa para que possa compensar na perca de energia
  double radius = 0.03;
  double naturalLength = 0.95;	
  double stiffness = 1000;		// acrescentado também o valor ao 'stiffnes'

  Mass m1(Vector2(-0.9,0.3), Vector2(), mass, radius) ; 	// inciando o vetor x e y com valores diferentes da massa 1 e massa 2
  Mass m2(Vector2(+0.5,0.4), Vector2(), mass, radius) ; 

  Spring spring(&m1, &m2, naturalLength, stiffness);

  springmass.addMass(&m1);			
  springmass.addMass(&m2);
  springmass.addSpring(&spring);

  run(&springmass, 1/120.0) ;
  return 0;
}
