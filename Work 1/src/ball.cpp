/** file: ball.cpp
 ** brief: Ball class - implementation
 ** author: Andrea Vedaldi
 ** modified by: Felipe Luis
 **/

#include "ball.h"

#include <iostream>

using namespace std;

Ball::Ball()
: r(0.1), x(0), y(0), vx(0.3), vy(-0.1), g(9.8), m(1),		// raio = 0.1	(x,y) = (0,0)	vel.x = 0.3		vel.y = -0.1	massa = 1	grav. = 9.8
xmin(-1), xmax(1), ymin(-1), ymax(1)						          // min.x = -1	max.x = 1		min.y = -1		max.y = 1
{ }															                          // assim, a dimenssão disponível para a bola é um retângulo de 2und x 2und

void Ball::setX()
{
  cout << "Entre com a posição inicial da bola em X: ";
  cin >> x;

  this -> x = x;
}

void Ball::setY()
{
  cout << "Entre com a posição inicial da bola em Y: ";
  cin >> y;

  this -> y = y;
}

void Ball::step(double dt)
{
	double xp = x + vx * dt;								         // equação horária da distância
	double yp = y + vy * dt - 0.5 * g * dt * dt;		 // equação horária movimento acelerado
 	
 	if(xmin + r <= xp && xp <= xmax - r)
 	{
    	x = xp;
  	}else
  	{
		vx = -vx;
  	}
  	if(ymin + r <= yp && yp <= ymax - r){
    	y = yp;
    	vy = vy - g * dt;
  	}
  	else
  	{
    	vy = -vy;
  	}
}

void Ball::display()
{
	std::cout << x << " " << y << std::endl;
}
